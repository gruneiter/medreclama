



export default function countdownBlock() {
  window.addEventListener('load', () => {
    const countdowns = document.querySelectorAll('.countdown');
    for (let item of countdowns) {
      let year = item.dataset.year;
      let month = item.dataset.month;
      let day = item.dataset.day;
      console.log(year);
      $(item).countdown({timestamp: new Date(year, month, day, 23, 59, 59)});
    }
  });
}
