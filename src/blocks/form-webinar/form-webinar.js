export default function formWebinar() {
  const fw = document.getElementById('form-webinar');
  if (fw) {
    const frs = Array.from(fw.querySelectorAll('.form__row'));
    frs.push(fw.querySelector('.form__submit'))
    fw.addEventListener('submit', (e) => {
      e.preventDefault();
      const floading = document.createElement('div');
      floading.className = 'form__loading';
      const fmess = document.createElement('div');
      fmess.className = 'form__message';
      fmess.innerText = 'Запись на вебинар закончена';
      for (let i = 0; i < frs.length; i++){
        frs[i].remove();
      }
      e.target.append(floading);
      setTimeout(() => {
        floading.remove();
        e.target.append(fmess);
      }, 1000)
    })
  }
}
