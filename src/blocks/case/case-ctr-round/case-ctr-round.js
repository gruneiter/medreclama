import circles from 'circles';

export default function caseCtrRound () {
  let circlesList = document.querySelectorAll('.case-ctr-round__diagram');
  let circlesSettings = {
    radius:              70,
    maxValue:            100,
    width:               30,
    text:                function(value){return 'CTR<br><strong>' + value + '%</strong>';},
    colors:              ['#cccccc', '#8c3052'],
    duration:            400,
    wrpClass:            'case-ctr-round__wrap',
    textClass:           'case-ctr-round__text',
    valueStrokeClass:    'case-ctr-round__valueStroke',
    maxValueStrokeClass: 'case-ctr-round__maxValueStroke',
    styleWrapper:        true,
    styleText:           false
  };
  for ( let item of circlesList) {
    circlesSettings.id = item.id;
    circlesSettings.value = item.dataset.count;
    let myCircle = circles.create(circlesSettings);
    console.log(item);
  }
}
