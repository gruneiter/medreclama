// import jquery from 'jquery';
import flexslider from 'flexslider';

export default function resultsCarousel() {
  window.addEventListener('load', () => {
    $('#results-carousel__content').flexslider({
      animation: "slide",
      directionNav: false,
      // controlNav: false,
      animationLoop: false,
      slideshow: false,
      sync: "#results-carousel__tabs"
    });
   
    $('#results-carousel__tabs').flexslider({
      animation: "slide",
      controlNav: false,
      directionNav: false,
      animationLoop: false,
      slideshow: false,
      asNavFor: '#results-carousel__content',
      direction: "vertical",
      minItems: 100
    });
    
    const carouselTabIndexes = document.querySelectorAll('.results-carousel__tab-label');
    let carouselTabCount = 0;
    for (let item of carouselTabIndexes) {
      if (carouselTabCount == 0) {
        item.classList.add('results-carousel__tab-label--active');
      }
      carouselTabCount++;
      item.addEventListener('click', () => {
        for (let i of carouselTabIndexes) {
          i.classList.remove('results-carousel__tab-label--active');
        }
        item.classList.add('results-carousel__tab-label--active');
      })
    }
  });
}
