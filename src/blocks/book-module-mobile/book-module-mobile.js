import Cookies from 'js-cookie';

export default function bookModuleMobile() {
  
  let bookModule = document.getElementById('book-module-mobile'); // модуль книги

  if(!bookModule){
    return false;
  }

  let bookModuleOpener = bookModule.querySelector('.book-module-mobile__open'); // кнопка открытия модуля
  let bookModuleCloser = bookModule.querySelector('.book-module-mobile__close'); // кнопка закрытия модуля
  let bookModuleForm = bookModule.querySelector('.book-module-mobile__form'); // форма в модуле
  let openerWasActive = false;

  // открытие модуля книги
  bookModuleOpener.addEventListener('click', (e) => {
    bookModule.classList.toggle('book-module-mobile--active');
    bookModuleOpener.classList.remove('book-module-mobile__open--active');
    return openerWasActive = true;
  });
  
  // закрытие модуля книги при клике на крестик
  bookModuleCloser.addEventListener('click', (e) => {
    bookModule.classList.remove('book-module-mobile--active');
  });
  
  // закрытие модуля книги при клике вне модуля 
  document.body.addEventListener('click', (e) => {
    let target = e.target;
    while (target !==  document.body) {
      if (target === bookModule) {
        return;
      }
      target = target.parentNode;
    }
    if (target === document.body && bookModule.classList.contains('book-module-mobile--active')) {
      bookModule.classList.remove('book-module-mobile--active');
    }
  });
  
  
  
  // заглушка для функции сабмита
  bookModuleForm.addEventListener('submit', (event) => {
    event.preventDefault();

    let bookModuleName = bookModuleForm.querySelector('.form-input[type="text"]');
    let bookModuleEmail = bookModuleForm.querySelector('.form-input[type="email"]');

    Cookies.set('sendBook_name', bookModuleName.value, {expires: 30});
    Cookies.set('sendBook_email', bookModuleEmail.value, {expires: 30});

  })

  document.addEventListener("DOMContentLoaded", () => {

      let isAutoShowed = false;
      if(Cookies.get('isAutoShowed') != undefined){
        isAutoShowed = true;
      }

      setTimeout(() => {
        if (!openerWasActive && !isAutoShowed) {
          bookModuleOpener.classList.add('book-module-mobile__open--active');
          openerWasActive = true;
          Cookies.set('isAutoShowed', true);
        }
      }, 7000)
  })

}
