export default function feedbackList() {
  const feedbackListReadMore = document.querySelectorAll('.feedback-list__read-more');
  for ( let item of feedbackListReadMore ) {
    item.addEventListener( 'click' , (e) => {
      let details = e.target.parentNode.querySelector('.feedback-list__details');
      details.classList.toggle('feedback-list__details--display');
      if (details.classList.contains('feedback-list__details--display')) {
        e.target.innerHTML = 'Скрыть';
      } else {
        e.target.innerHTML = 'Подробнее';
      }
    } );
  }
}
