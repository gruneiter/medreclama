export default function bookModule() {
  
  let bookModule = document.getElementById('book-module'); // модуль книги

  if(!bookModule){
    return false;
  }

  let bookModuleOpener = bookModule.querySelector('.book-module__open'); // кнопка открытия модуля
  let bookModuleCloser = bookModule.querySelector('.book-module__close'); // кнопка закрытия модуля
  let bookModuleForm = bookModule.querySelector('.book-module__form'); // форма в модуле
  let openerWasActive = false;
  
  // открытие модуля книги
  bookModuleOpener.addEventListener('click', (e) => {
    bookModule.classList.toggle('book-module--active');
    return openerWasActive = true;
  });
  
  // закрытие модуля книги при клике на крестик
  bookModuleCloser.addEventListener('click', (e) => {
    bookModule.classList.remove('book-module--active');
  });
  
  // закрытие модуля книги при клике вне модуля 
  document.body.addEventListener('click', (e) => {
    let target = e.target;
    while (target !==  document.body) {
      if (target === bookModule) {
        return;
      }
      target = target.parentNode;
    }
    if (target === document.body && bookModule.classList.contains('book-module--active')) {
      bookModule.classList.remove('book-module--active');
    }
  });
  
  // заглушка для функции сабмита
  // bookModuleForm.addEventListener('submit', (event) => {
  //   event.preventDefault();
  // 
  //   let bookModuleName = bookModuleForm.querySelector('.form-input[type="text"]');
  //   let bookModuleEmail = bookModuleForm.querySelector('.form-input[type="email"]');
  // 
  //   Cookies.set('sendBook_name', bookModuleName.value, {expires: 30});
  //   Cookies.set('sendBook_email', bookModuleEmail.value, {expires: 30});
  // })
  
  
  document.addEventListener("DOMContentLoaded", () => {
  
      let isAutoShowed = false;
      if(Cookies.get('isAutoShowed') != undefined){
        isAutoShowed = true;
      }
  
      setTimeout(() => {
        if (!openerWasActive && !isAutoShowed) {
          bookModule.classList.add('book-module--active');
          openerWasActive = true;
          Cookies.set('isAutoShowed', true);
        }
      }, 7000)
  
  })

}
