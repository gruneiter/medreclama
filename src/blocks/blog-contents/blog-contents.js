export default function bc() {
  const blogArticle = document.querySelector('.blog-article');
  if (blogArticle) {
    const blogArticleHeaders = Array.from(blogArticle.children);
    const h = [];
    const sh = [];
    let h2 = 0;
    let h3 = 0;
    let parent = 0;
    blogArticleHeaders.forEach((c)=>{
      if (c.matches('h2')) {
        parent += 1;
        if (!c.id) {
          c.id = `header-2-${h2}`;
          h2 += 1;
        };
        h.push({
          content: c.innerHTML,
          href: c.id,
        });
      };
      if (c.matches('h3')) {
        if (!c.id) {
          c.id = `header-3-${h3}`;
          h3 += 1;
        };
        sh.push({
          content: c.innerHTML,
          id:c.id,
          parent: parent - 1,
        });
      };
    });
    sh.forEach((item) => {
      const p = item.parent;
      h[p].children = h[p].children || [];
      h[p].children.push({content: item.content, href: item.id});
    })
    if (h) {
      const createItem = (elem) => {
        const item = document.createElement('li');
        const link = document.createElement('a');
        link.href = `#${elem.href}`;
        link.innerHTML = elem.content;
        item.className = 'blog-contents__item';
        item.append(link);
        return item
      }
      const createList = (className) => {
        const list = document.createElement('ol');
        list.className = `blog-contents__list`;
        return list;
      }
      const list = createList();
      h.forEach((item) => {
        let i = createItem(item);
        if (item.children) {
          const sublist = createList();
          item.children.forEach((child) => {
            sublist.append(createItem(child));
          });
          i.append(sublist);
        }
        list.append(i);
      });
      const blogContents = document.createElement('div');
      blogContents.className = `blog-contents`;
      blogContents.innerHTML = `<div class="blog-contents__title">Содержание</div>`
      blogContents.append(list)
      blogArticle.prepend(blogContents);
    }
  }
}
