import circles from 'circles';

export default function circlesDraw () {
  let circlesList = document.querySelectorAll('.figure-diagram__circle');
  let circlesSettings = {
    radius:              80,
    maxValue:            100,
    width:               8,
    text:                function(value){return value + '%';},
    colors:              ['#cccccc', '#734d76'],
    duration:            400,
    wrpClass:            'figure-diagram__wrap',
    textClass:           'figure-diagram__text',
    valueStrokeClass:    'figure-diagram__valueStroke',
    maxValueStrokeClass: 'figure-diagram__maxValueStroke',
    styleWrapper:        true,
    styleText:           true
  };
  for ( let item of circlesList) {
    circlesSettings.id = item.id;
    circlesSettings.value = item.dataset.count;
    let myCircle = circles.create(circlesSettings);
  }
}
