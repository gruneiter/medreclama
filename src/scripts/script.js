//import jquery from 'jquery';
import flexslider from 'flexslider';
//import fancybox from '@fancyapps/fancybox';
import circles from 'circles';
import countdown from './jquery.countdown.js';

import headerMobileMenu from '../blocks/header-mobile/header-mobile-menu/header-mobile-menu';
import circlesDraw from '../blocks/figure-diagram/figure-diagram';
import resultsCarousel from '../blocks/results-carousel/results-carousel';
import countdownBlock from '../blocks/countdown/countdown';
import caseCtrRound from '../blocks/case/case-ctr-round/case-ctr-round.js';
import feedbackList from '../blocks/feedback-list/feedback-list.js';
import bookModule from '../blocks/book-module/book-module.js';
import bookModuleMobile from '../blocks/book-module-mobile/book-module-mobile.js';
import formWebinar from '../blocks/form-webinar/form-webinar.js';
import blogContents from '../blocks/blog-contents/blog-contents.js';


// import circles from '../../node_modules/circles/circles.js';


headerMobileMenu();
circlesDraw();
resultsCarousel();
countdownBlock();
caseCtrRound();
feedbackList();
bookModule()
bookModuleMobile()
formWebinar();
blogContents();

window.addEventListener('load', () => {
  $('.home-feedback__slides').flexslider({
    animation: "slide",
    controlNav: false,
    prevText: "",
    nextText: "",
    slideshow: false
  });
  $('.result-slider').flexslider({
    animation: "slide",
    controlNav: true,
    prevText: "",
    nextText: "",
    slideshow: false
  });
});
