import gulp from 'gulp';
import gulpIf from 'gulp-if';
import stylus from 'gulp-stylus';
import pug from 'gulp-pug';
import stylint from 'gulp-stylint';
import cssNano from 'gulp-cssnano';
import sourcemaps from 'gulp-sourcemaps';
import browserSync from 'browser-sync';
import watch from 'gulp-watch';
import gcmq from 'gulp-group-css-media-queries';
import changed from 'gulp-changed';
import autoprefixer from 'autoprefixer-stylus';
import importIfExist from 'stylus-import-if-exist';
import plumber from 'gulp-plumber';
import webpack from 'webpack';
import webpackStream from 'webpack-stream';
import webpackConf from './webpack.config.js';
import merge from 'merge-stream';
import stylusSvgImport from 'stylus-svg';
import runSequence from 'run-sequence';
import inheritance from 'gulp-pug-inheritance';
import cached from 'gulp-cached';
import filter from 'gulp-filter';
import rename from 'gulp-rename';

var reload = browserSync.reload;

const isDebug = process.env.NODE_ENV !== 'production';

const distDir = './dist';
const templateDir = `${distDir}/template/`;

const productionImages = '../../public_html/images/';
const productionTemplate = '../../public_html/templates/tpl_medreclama/';

gulp.task('stylus', () => {
  gulp.src('./src/styles/*.styl')
    .pipe(plumber())
    .pipe(gulpIf(isDebug, sourcemaps.init()))
    .pipe(stylus({
      use: [
        stylusSvgImport(),
        importIfExist(),
        autoprefixer()
      ],
      'include css': true
    }))
    .pipe(gulpIf(!isDebug, gcmq()))
    .pipe(gulpIf(isDebug, sourcemaps.write()))
    .pipe(gulp.dest(`${templateDir}styles`))
});

gulp.task('stylint', () => (
  gulp.src(['src/**/*.styl', '!src/styles/**'])
    .pipe(stylint())
    .pipe(stylint.reporter())
));


gulp.task('template', () => {
  gulp.src('./src/pages/**/*.pug')
    .pipe(plumber())
    .pipe(cached('template'))
    .pipe(gulpIf(global.isWatching, inheritance({basedir: 'src', skip: 'node_modules'})))
    .pipe(filter(file => /src[\\\/]pages/.test(file.path)))
    .pipe(pug( {
      basedir: 'src',
      pretty: true
    } ))
    .pipe(rename(function (path) {
      path.dirname = path.dirname.startsWith('pages') ? '.' + path.dirname.slice(5) : path.dirname
    }))
    .pipe(gulp.dest(distDir));
  
});

gulp.task('copy', () => {
  gulp.src(['src/resources/**'])
    .pipe(changed(distDir))
    .pipe(gulp.dest(distDir));
  gulp.src('src/template/**')
    .pipe(changed(`${templateDir}`))
    .pipe(gulp.dest(`${templateDir}`));
});

gulp.task('copyNodeModules', () => {
  gulp.src('node_modules/jquery/dist/jquery.min.js')
    .pipe(changed(`${templateDir}scripts/`))
    .pipe(gulp.dest(`${templateDir}scripts/`));
})

gulp.task('script', () => {
  gulp.src('./src/scripts/script.js')
    .pipe(plumber())
    .pipe(webpackStream(webpackConf, webpack))
    .pipe(gulp.dest(`${templateDir}scripts/`));
})

gulp.task('watcher', () => {
	global.isWatching = true;
  watch(['./src/styles/*.styl','./src/styles/includes/*', './src/styles/svg/*.svg', './src/blocks/**/*.styl', './src/blocks/**/**/*.styl'], () => runSequence('stylint','stylus', reload));
  watch(['./src/blocks/**/*.pug', './src/blocks/**/**/*.pug', './src/pages/**/*.pug', './src/pages/**/**/*.pug', './src/data/*'], () => runSequence('template', reload));
  watch(['./src/blocks/**/*.js', './src/scripts/script.js'], () => runSequence('script', reload));
  watch(['./src/resources/**','src/template/**'], () => runSequence('copy', reload));
});

gulp.task('build', ['stylint', 'stylus', 'template', 'script', 'copy', 'copyNodeModules'])

gulp.task('browserSync', ()  => {
  browserSync({
    server: {
      baseDir: [
        "dist"
      ]
    },
    open: false,
    notify: false
  });
});

gulp.task('default', () => (runSequence('stylint', 'stylus', 'template', 'script',  'copy', 'copyNodeModules', 'browserSync', 'watcher')));

/********** Build to site directory **********/
const distDirSite = '../../public_html';
const templateDirSite = `${distDirSite}/bitrix/templates/.default/`;


gulp.task('copy_local_resource', () => {
  process.env.NODE_ENV = process.env.NODE_ENV ? process.env.NODE_ENV : 'copySite';
  return gulp.src(['src/resources/**'])
    .pipe(changed(distDirSite))
    .pipe(gulp.dest(distDirSite))
    .pipe(reload({stream:true}));
});

gulp.task('copy_local_template', () => {
  return gulp.src('dist/template/**')
    .pipe(changed(`${templateDirSite}`))
    .pipe(gulp.dest(`${templateDirSite}`))
    .pipe(reload({stream:true}));
});
gulp.task('build_local', ['stylus', 'template', 'script', 'copy', 'copy_local_resource', 'copy_local_template']);
/********** /Build to site directory **********/
